<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/language/{id}', function ($id) {
    $lang = \App\Languages::find($id);
    return view('language', compact('lang'));
});
Route::get('/word/{language_id}', function ($language_id) {
    $lang = \App\Words::where('language_id', '=' ,$language_id)->simplePaginate(1);
    return view('words', compact('lang'));
});

Route::get('/sentence/{language_id}', function ($language_id) {
    $lang = \App\Sentences::where('language_id', '=' ,$language_id)->simplePaginate(1);
    return view('sentences', compact('lang'));
});

Route::post('/upload', function (Request $request) {
    if (!file_exists(public_path('uploads'))) {
            mkdir(public_path('uploads'), 0777);
            mkdir(public_path('uploads/thumb'), 0777);
        }
        // echo '<pre>';
        // var_dump($request->all());
        // die();
        foreach (Request::all() as $key => $value) {
            if (Request::hasFile($key)) {
                $filename = time() . '-' . Request::file($key)->getClientOriginalName();
                    Request::file($key)->move(public_path('uploads'), $filename);
                    //$request = new Request(array_merge($request->all(), [$key => $filename]));
            }
        }

        return $filename;
});

