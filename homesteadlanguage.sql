-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2017 at 06:29 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `homesteadlanguage`
--

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Arabic', '2017-08-12 05:30:34', '2017-08-12 05:30:34', NULL),
(2, 'Italian', '2017-08-12 05:30:41', '2017-08-12 05:30:41', NULL),
(3, 'Bangla', '2017-08-12 05:31:01', '2017-08-12 05:31:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL,
  `position` int(11) DEFAULT NULL,
  `menu_type` int(11) NOT NULL DEFAULT '1',
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `position`, `menu_type`, `icon`, `name`, `title`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 0, NULL, 'User', 'User', NULL, NULL, NULL),
(2, NULL, 0, NULL, 'Role', 'Role', NULL, NULL, NULL),
(3, 0, 1, 'fa-database', 'Languages', 'Language', NULL, '2017-08-12 05:29:59', '2017-08-12 05:29:59'),
(4, 0, 1, 'fa-database', 'Words', 'Word', NULL, '2017-08-21 08:22:18', '2017-08-21 08:22:18'),
(5, 0, 1, 'fa-database', 'Sentences', 'Sentence', NULL, '2017-08-22 12:00:43', '2017-08-22 12:00:43');

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE IF NOT EXISTS `menu_role` (
  `menu_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`menu_id`, `role_id`) VALUES
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_10_10_000000_create_menus_table', 1),
(4, '2015_10_10_000000_create_roles_table', 1),
(5, '2015_10_10_000000_update_users_table', 1),
(6, '2015_12_11_000000_create_users_logs_table', 1),
(7, '2016_03_14_000000_update_menus_table', 1),
(8, '2017_08_12_112959_create_languages_table', 2),
(9, '2017_08_21_142218_create_words_table', 3),
(10, '2017_08_22_180043_create_sentences_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2017-08-09 11:24:52', '2017-08-09 11:24:52'),
(2, 'User', '2017-08-09 11:24:52', '2017-08-09 11:24:52');

-- --------------------------------------------------------

--
-- Table structure for table `sentences`
--

CREATE TABLE IF NOT EXISTS `sentences` (
  `id` int(10) unsigned NOT NULL,
  `english_sentence` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `tutorial_sentence` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sentences`
--

INSERT INTO `sentences` (`id`, `english_sentence`, `language_id`, `tutorial_sentence`, `audio`, `meta_data`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'There were 10 in his bed and the little one said Roll over! Roll over! ', 2, 'ache', '10bears.mp3', '[\r\n            { "end": "0.225","start": "0.125","text": "There" },\r\n            {"end": "0.485","start": "0.225","text": "were" },\r\n            {"end": "1.085","start": "0.485","text": "10 in" },\r\n            {"end": "1.325","start": "1.085","text": "his" },\r\n            {"end": "1.685","start": "1.325","text": "bed" },\r\n            {"end": "1.965","start": "1.685","text": "and the" },\r\n            {"end": "2.245","start": "1.965","text": "little" },\r\n            {"end": "2.565","start": "2.245","text": "one" },\r\n            {"end": "2.985","start": "2.565","text": "said" },\r\n            {"end": "3.485","start": "2.985","text": "Roll" },\r\n            {"end": "3.965","start": "3.485","text": "over!" },\r\n            {"end": "4.805","start": "3.965","text": "Roll" },\r\n            {"end": "5.405","start": "4.805","text": "over!" }\r\n          ]', '2017-08-22 13:47:03', '2017-08-22 13:47:03', NULL),
(5, 'sd sdt', 2, 'sd sdt', '1503464046-apple.mp3', '[{"end":"0.25","start":"0.23","text":"sd"},{"end":"1.25","start":"0.26","text":"sdt"}]', '2017-08-22 22:55:21', '2017-08-22 22:55:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@example.com', '$2y$10$ohPLvmRBjng/d7Advywb1.Qu7m5e/jI2fi.t21MzuyTXom6au1/dG', 'Pr5if9HcjS9C5km4Bawue9JlSr3qdRPcbWtedyfDm1A3EvWRU9W8MIUsF3UG', '2017-08-09 11:25:04', '2017-08-09 11:25:04');

-- --------------------------------------------------------

--
-- Table structure for table `users_logs`
--

CREATE TABLE IF NOT EXISTS `users_logs` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_logs`
--

INSERT INTO `users_logs` (`id`, `user_id`, `action`, `action_model`, `action_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'updated', 'users', 1, '2017-08-09 11:27:29', '2017-08-09 11:27:29'),
(2, 1, 'created', 'languages', 1, '2017-08-12 05:30:34', '2017-08-12 05:30:34'),
(3, 1, 'created', 'languages', 2, '2017-08-12 05:30:41', '2017-08-12 05:30:41'),
(4, 1, 'created', 'languages', 3, '2017-08-12 05:31:01', '2017-08-12 05:31:01'),
(5, 1, 'created', 'words', 1, '2017-08-21 10:26:03', '2017-08-21 10:26:03'),
(6, 1, 'created', 'words', 2, '2017-08-21 10:33:19', '2017-08-21 10:33:19'),
(7, 1, 'created', 'words', 3, '2017-08-21 10:34:39', '2017-08-21 10:34:39'),
(8, 1, 'created', 'words', 4, '2017-08-21 12:30:32', '2017-08-21 12:30:32'),
(9, 1, 'created', 'words', 5, '2017-08-21 12:53:16', '2017-08-21 12:53:16'),
(10, 1, 'created', 'words', 6, '2017-08-22 10:52:53', '2017-08-22 10:52:53'),
(11, 1, 'created', 'words', 7, '2017-08-22 11:44:50', '2017-08-22 11:44:50'),
(12, 1, 'created', 'sentences', 1, '2017-08-22 12:15:09', '2017-08-22 12:15:09'),
(13, 1, 'created', 'sentences', 1, '2017-08-22 12:58:58', '2017-08-22 12:58:58'),
(14, 1, 'created', 'sentences', 2, '2017-08-22 13:02:15', '2017-08-22 13:02:15'),
(15, 1, 'created', 'sentences', 3, '2017-08-22 13:47:03', '2017-08-22 13:47:03'),
(16, 1, 'created', 'words', 8, '2017-08-22 14:14:30', '2017-08-22 14:14:30'),
(17, 1, 'created', 'sentences', 4, '2017-08-22 22:52:47', '2017-08-22 22:52:47'),
(18, 1, 'created', 'sentences', 5, '2017-08-22 22:55:22', '2017-08-22 22:55:22'),
(19, 1, 'created', 'words', 9, '2017-08-23 00:54:55', '2017-08-23 00:54:55'),
(20, 1, 'created', 'words', 10, '2017-08-23 06:21:04', '2017-08-23 06:21:04'),
(21, 1, 'created', 'words', 11, '2017-08-30 06:15:58', '2017-08-30 06:15:58'),
(22, 1, 'created', 'words', 12, '2017-08-30 06:17:45', '2017-08-30 06:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE IF NOT EXISTS `words` (
  `id` int(10) unsigned NOT NULL,
  `word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_translation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_word` text COLLATE utf8mb4_unicode_ci,
  `tutorial_translation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tutorial_word` text COLLATE utf8mb4_unicode_ci,
  `difference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `difference_word` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `words`
--

INSERT INTO `words` (`id`, `word`, `language_id`, `image`, `english_translation`, `english_word`, `tutorial_translation`, `tutorial_word`, `difference`, `difference_word`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'apple', 2, '1503432870-apple.jpg', '1503432860-apple.mp3', '', '1503432862-apple.mp3', '', '1503432864-apple.mp3', '', '2017-08-22 14:14:30', '2017-08-22 14:14:30', NULL),
(9, 'Leaf', 2, '1503471295-7579213_orig.jpg', '1503471290-apple.mp3', '', '1503471292-apple.mp3', '', '1503471294-apple.mp3', '', '2017-08-23 00:54:55', '2017-08-23 00:54:55', NULL),
(10, 'Leaf', 2, '1503490863-nat.jpg', '1503490847-apple.mp3', '', '1503490851-apple.mp3', '', '1503490854-apple.mp3', '', '2017-08-23 06:21:04', '2017-08-23 06:21:04', NULL),
(11, 'df', 2, '1504095357-apple.jpg', '1504095346-apple.mp3', NULL, '1504095348-apple.mp3', NULL, '1504095350-apple.mp3', NULL, '2017-08-30 06:15:58', '2017-08-30 06:15:58', NULL),
(12, 'sfcsfc', 2, '1504095465-7579213_orig.jpg', '1504095458-apple.mp3', 'zc', '1504095460-apple.mp3', 'zx', '1504095462-apple.mp3', 'zx', '2017-08-30 06:17:45', '2017-08-30 06:17:45', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_role`
--
ALTER TABLE `menu_role`
  ADD UNIQUE KEY `menu_role_menu_id_role_id_unique` (`menu_id`,`role_id`), ADD KEY `menu_role_menu_id_index` (`menu_id`), ADD KEY `menu_role_role_id_index` (`role_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sentences`
--
ALTER TABLE `sentences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_logs`
--
ALTER TABLE `users_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `words`
--
ALTER TABLE `words`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sentences`
--
ALTER TABLE `sentences`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_logs`
--
ALTER TABLE `users_logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `words`
--
ALTER TABLE `words`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu_role`
--
ALTER TABLE `menu_role`
ADD CONSTRAINT `menu_role_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `menu_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
