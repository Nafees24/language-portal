<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Sentences extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'sentences';
    
    protected $fillable = [
          'english_sentence',
          'meta_data',
          'language_id',
          'tutorial_sentence',
          'audio'
    ];
    

    public static function boot()
    {
        parent::boot();

        Sentences::observe(new UserActionsObserver);
    }
    
    
    
    
}