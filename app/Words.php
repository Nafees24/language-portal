<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Words extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'words';
    
    protected $fillable = [
          'word',
          'image',
          'english_word',
          'tutorial_word',
          'difference_word',
          'language_id',
          'english_translation',
          'tutorial_translation',
          'difference'
    ];
    

    public static function boot()
    {
        parent::boot();

        Words::observe(new UserActionsObserver);
    }
    
    
    
    
}