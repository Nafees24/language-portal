<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Languages;
use App\Http\Requests\CreateLanguagesRequest;
use App\Http\Requests\UpdateLanguagesRequest;
use Illuminate\Http\Request;



class LanguagesController extends Controller {

	/**
	 * Display a listing of languages
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $languages = Languages::all();

		return view('admin.languages.index', compact('languages'));
	}

	/**
	 * Show the form for creating a new languages
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.languages.create');
	}

	/**
	 * Store a newly created languages in storage.
	 *
     * @param CreateLanguagesRequest|Request $request
	 */
	public function store(CreateLanguagesRequest $request)
	{
	    
		Languages::create($request->all());

		return redirect()->route(config('quickadmin.route').'.languages.index');
	}

	/**
	 * Show the form for editing the specified languages.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$languages = Languages::find($id);
	    
	    
		return view('admin.languages.edit', compact('languages'));
	}

	/**
	 * Update the specified languages in storage.
     * @param UpdateLanguagesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateLanguagesRequest $request)
	{
		$languages = Languages::findOrFail($id);

        

		$languages->update($request->all());

		return redirect()->route(config('quickadmin.route').'.languages.index');
	}

	/**
	 * Remove the specified languages from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Languages::destroy($id);

		return redirect()->route(config('quickadmin.route').'.languages.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Languages::destroy($toDelete);
        } else {
            Languages::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.languages.index');
    }

}
