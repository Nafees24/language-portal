<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Sentences;
use App\Http\Requests\CreateSentencesRequest;
use App\Http\Requests\UpdateSentencesRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;


class SentencesController extends Controller {

	/**
	 * Display a listing of sentences
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $sentences = Sentences::all();

		return view('admin.sentences.index', compact('sentences'));
	}

	/**
	 * Show the form for creating a new sentences
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.sentences.create');
	}

	/**
	 * Store a newly created sentences in storage.
	 *
     * @param CreateSentencesRequest|Request $request
	 */
	public function store(CreateSentencesRequest $request)
	{
	    $request = $this->saveFiles($request);

	    $meta_data = [];

	    $word = $request->input('word');
	    $start = $request->input('start');
	    $end = $request->input('end');

	    foreach ($word as $key => $value) {
	    	$meta_data[] = array(
	    					'end' => $end[$key],
	    					'start' => $start[$key],
	    					'text' => $value,
	    					);
	    }

	    //die(json_encode($meta_data));
	    var_dump($request->all());
	    $request = new Request(array_merge($request->all(), ['meta_data' => json_encode($meta_data)]));

		Sentences::create($request->all());

		return redirect()->route(config('quickadmin.route').'.sentences.index');
	}

	/**
	 * Show the form for editing the specified sentences.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$sentences = Sentences::find($id);
	    
	    
		return view('admin.sentences.edit', compact('sentences'));
	}

	/**
	 * Update the specified sentences in storage.
     * @param UpdateSentencesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateSentencesRequest $request)
	{
		$sentences = Sentences::findOrFail($id);

        $request = $this->saveFiles($request);

		$sentences->update($request->all());

		return redirect()->route(config('quickadmin.route').'.sentences.index');
	}

	/**
	 * Remove the specified sentences from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Sentences::destroy($id);

		return redirect()->route(config('quickadmin.route').'.sentences.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Sentences::destroy($toDelete);
        } else {
            Sentences::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.sentences.index');
    }

}
