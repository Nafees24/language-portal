<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Words;
use App\Http\Requests\CreateWordsRequest;
use App\Http\Requests\UpdateWordsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;


class WordsController extends Controller {

	/**
	 * Display a listing of words
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $words = Words::all();

		return view('admin.words.index', compact('words'));
	}

	/**
	 * Show the form for creating a new words
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.words.create');
	}

	/**
	 * Store a newly created words in storage.
	 *
     * @param CreateWordsRequest|Request $request
	 */
	public function store(CreateWordsRequest $request)
	{
	    $request = $this->saveFiles($request);
		Words::create($request->all());

		return redirect()->route(config('quickadmin.route').'.words.index');
	}

	/**
	 * Show the form for editing the specified words.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$words = Words::find($id);
	    
	    
		return view('admin.words.edit', compact('words'));
	}

	/**
	 * Update the specified words in storage.
     * @param UpdateWordsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateWordsRequest $request)
	{
		$words = Words::findOrFail($id);

        $request = $this->saveFiles($request);

		$words->update($request->all());

		return redirect()->route(config('quickadmin.route').'.words.index');
	}

	/**
	 * Remove the specified words from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Words::destroy($id);

		return redirect()->route(config('quickadmin.route').'.words.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Words::destroy($toDelete);
        } else {
            Words::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.words.index');
    }

}
