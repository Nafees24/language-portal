<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateWordsRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'word' => 'required', 
            'image' => 'required',
            'english_word' => 'required',
            'tutorial_word' => 'required',
            'difference_word' => 'required',
            'language_id' => 'required',
            'english_translation' => 'required', 
            'tutorial_translation' => 'required', 
            'difference' => 'required', 
            
		];
	}
}
