<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="{{ url('quickadmin/js') }}/timepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
<script src="{{ url('quickadmin/js') }}/bootstrap.min.js"></script>
<script src="{{ url('quickadmin/js') }}/main.js"></script>

<script>

    $('.datepicker').datepicker({
        autoclose: true,
        dateFormat: "{{ config('quickadmin.date_format_jquery') }}"
    });

    $('.datetimepicker').datetimepicker({
        autoclose: true,
        dateFormat: "{{ config('quickadmin.date_format_jquery') }}",
        timeFormat: "{{ config('quickadmin.time_format_jquery') }}"
    });

    $('#datatable').dataTable( {
        "language": {
            "url": "{{ trans('quickadmin::strings.datatable_url_language') }}"
        }
    });

</script>

<script type="text/javascript">

$( document ).ready(function() {

 
$("#player_audio").click(function() {
            alert(1);
              if (this.paused == false) {
                $("#wordtitle").html('df');
              }
        });


    //when the Add Field button is clicked
$("#add").click(function (e) {
//Append a new row of code to the "#items" div
  $("#items").append('<div class="form-group"><label for="tutorial_sentence" class="col-sm-2 control-label">Word</label><div class="col-sm-2"><input class="form-control" name="word[]" type="text"></div><label for="tutorial_sentence" class="col-sm-2 control-label">Start</label><div class="col-sm-2"><input class="form-control" name="start[]" type="text"></div><label for="tutorial_sentence" class="col-sm-2 control-label">End</label><div class="col-sm-2"><input class="form-control" name="end[]" type="text"></div></div><a  onClick="$(this).prev().remove();$(this).remove();"class="rem btn btn-danger del">Delete</a>');
});

    $(".fileuploadcus").change(function() {
        //alert($(this).attr('id'));
        var tt = $(this).attr('id');
         var formData = new FormData();
        formData.append('file', $(this)[0].files[0]);

        $.ajax({
               url : '{{ url("/upload") }}',
               type : 'POST',
               data : formData,
               processData: false,  // tell jQuery not to process the data
               contentType: false,  // tell jQuery not to set contentType
               success : function(data) {
                   
                   $("#"+tt+"_hid").val(data);
                   var playme = document.getElementById('audiofile'); 
                   playme.src="{{ asset('uploads') . '/' }}"+data; 
                   playme.load();
                   $("#audiofile").show();
                   //alert("#"+tt+"_hid");
               }
        });
    });
});



    
</script>
