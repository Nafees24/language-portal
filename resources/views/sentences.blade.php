<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif




            <div class="content">

           

            @foreach ($lang as $row)

             <textarea id="textarea" style="width:200px;height:200px;display: none;">{{$row->english_sentence}}</textarea><br>
<input type="button" id="playbtn" value="Play" class="btn btn-primary" /><br>
Speaking word : <span id="word" style="color:blue;"></span><br><br>
<div id="panel" style="min-width:300px;word-wrap:wrap;display:inline-block;">

</div>
            <h4>{{$row->tutorial_sentence}}</h4>
                <div class="title m-b-md">
                    <audio id="audiofile" src="{{ asset('uploads') . '/'.  $row->audio }}" controls></audio><br>
        <div id="subtitles"></div>
        <script>
        ( function(win, doc) {

            var utterance = new SpeechSynthesisUtterance();
var wordIndex = 0;
var global_words = [];
utterance.lang = 'en-UK';
utterance.rate = 1;


document.getElementById('playbtn').onclick = function(){
    var text    = document.getElementById('textarea').value;
    var words   = text.split(" ");
    global_words = words;
    // Draw the text in a div
    drawTextInPanel(words);
    spokenTextArray = words;
    utterance.text = text;
    speechSynthesis.speak(utterance);
};

utterance.onboundary = function(event){
    var e = document.getElementById('textarea');
    var word = getWordAt(e.value,event.charIndex);
    // Show Speaking word : x
    document.getElementById("word").innerHTML = word;
    //Increase index of span to highlight
    console.info(global_words[wordIndex]);
    
    try{
        document.getElementById("word_span_"+wordIndex).style.color = "red";
    }catch(e){}
    
    wordIndex++;
};

utterance.onend = function(){
    document.getElementById("word").innerHTML = "";
     wordIndex = 0;
     document.getElementById("panel").innerHTML = "";
     location.reload();
};

// Get the word of a string given the string and the index
function getWordAt(str, pos) {
    // Perform type conversions.
    str = String(str);
    pos = Number(pos) >>> 0;

    // Search for the word's beginning and end.
    var left = str.slice(0, pos + 1).search(/\S+$/),
        right = str.slice(pos).search(/\s/);

    // The last word in the string is a special case.
    if (right < 0) {
        return str.slice(left);
    }
    // Return the word, using the located bounds to extract it from the string.
    return str.slice(left, right + pos);
}

function drawTextInPanel(words_array){
console.log("Dibujado");
        var panel = document.getElementById("panel");
    for(var i = 0;i < words_array.length;i++){
        var html = '<span id="word_span_'+i+'">'+words_array[i]+'</span>&nbsp;';
        panel.innerHTML += html;
    }
}


//             var utterance = new SpeechSynthesisUtterance();
//             var wordIndex = 0;
//             var global_words = [];
//             utterance.lang = 'en-UK';
//             utterance.rate = 0.6;


// document.getElementById('playbtn').onclick = function(){
//     var text    = '{{$row->english_sentence}}';
//     var words   = text.split(" ");
//     global_words = words;
//     // Draw the text in a div
//     drawTextInPanel(words);
//     spokenTextArray = words;
//     utterance.text = text;
//     speechSynthesis.speak(utterance);
// };

// utterance.onboundary = function(event){
//     var e = document.getElementById('textarea');
//     var word = getWordAt(e.value,event.charIndex);
//     // Show Speaking word : x
//     document.getElementById("word").innerHTML = word;
//     //Increase index of span to highlight
//     console.info(global_words[wordIndex]);
    
//     try{
//         //document.getElementById("word_span_"+wordIndex).style.color = "blue";
//         $("#word_span_"+wordIndex).css("color", "red");
//     }catch(e){}
    
//     wordIndex++;
// };

// utterance.onend = function(){
//         document.getElementById("word").innerHTML = "";
//     wordIndex = 0;
//     document.getElementById("panel").innerHTML = "";
// };

// // Get the word of a string given the string and the index
// function getWordAt(str, pos) {
//     // Perform type conversions.
//     str = String(str);
//     pos = Number(pos) >>> 0;

//     // Search for the word's beginning and end.
//     var left = str.slice(0, pos + 1).search(/\S+$/),
//         right = str.slice(pos).search(/\s/);

//     // The last word in the string is a special case.
//     if (right < 0) {
//         return str.slice(left);
//     }
//     // Return the word, using the located bounds to extract it from the string.
//     return str.slice(left, right + pos);
// }

// function drawTextInPanel(words_array){
// console.log("Dibujado");
//         var panel = document.getElementById("panel");
//     for(var i = 0;i < words_array.length;i++){
//         var html = '<span id="word_span_'+i+'">'+words_array[i]+'</span>&nbsp;';
//         panel.innerHTML += html;
//     }
// }
            // var audioPlayer = doc.getElementById("audiofile");
            // var subtitles = doc.getElementById("subtitles");
            // var syncData = <?php echo json_encode($row->meta_data, JSON_UNESCAPED_SLASHES); ?>;
            // syncData = JSON.parse(syncData);
            // createSubtitle();

            // function createSubtitle()
            // {
            //     var element;
            //     for (var i = 0; i < syncData.length; i++) {
            //         element = doc.createElement('span');
            //         element.setAttribute("id", "c_" + i);
            //         element.innerText = syncData[i].text + " ";
            //         subtitles.appendChild(element);
            //     }
            // }

            // audioPlayer.addEventListener("timeupdate", function(e){
            //     syncData.forEach(function(element, index, array){
            //         if( audioPlayer.currentTime >= element.start && audioPlayer.currentTime <= element.end )
            //             subtitles.children[index].style.background = 'yellow';
            //     });
            // });
            // audioPlayer.addEventListener("ended", function(e){
            //     syncData.forEach(function(element, index, array){
            //          subtitles.children[index].style.background = 'white';
            //     });
            // });
        }(window, document));
        </script>
                </div>
                @endforeach

                <div class="links">
                   {{ $lang->links() }}
                </div>
            </div>
        </div>
    </body>
</html>
